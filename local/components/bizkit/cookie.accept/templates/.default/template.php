<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
CJSCore::Init();
?>
<?if($arResult['COOKIE_AGREE']!='Y'):?>
    <section id="cookie-modal" class="cookie-notification-modal">
        <div class="container">
            <div class="cookie-agree">
                <p class="cookie-agree__text">
                    <?=$arParams['COOKIE_TEXT']?>
                    <?if(!empty($arParams['COOKIE_AGREE_LINK'])):?>
                        <a href="<?=$arParams['COOKIE_AGREE_LINK']?>" class="cookie-more" target="_blank">
                            <?=GetMessage("COOKIE_TEXT_POLICY");?>
                        </a>
                    <?endif;?>
                </p>
                <div class="cookie-agree__buttons">
                    <div class="cookie-agree__button btn-red js-coockie-agree"><?=GetMessage("COOKIE_TEXT_OK");?></div>
                </div>
            </div>
        </div>
    </section>

    <script type="text/javascript">
        BX.ready(function(){
            BX.bindDelegate(
                document.body, 'click', {className: 'js-coockie-agree' },
                function(e){
                    if(!e) {
                        e = window.event;
                    }
                    BX.setCookie('<?=\Bitrix\Main\Config\Option::get('main', 'cookie_name', 'BITRIX_SM')?>_<?=$arParams['COOKIE_NAME']?>', 'Y', {expires: <?=$arParams['COOKIE_TIME']?>,path:"/"});
                    BX.style(BX('cookie-modal'), 'display', 'none');
                    return BX.PreventDefault(e);
                }
            );
        });
    </script>
<?endif;?>