<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
    "GROUPS" => array(
    ),
    "PARAMETERS"  =>  array(
        "COOKIE_NAME"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_COOKIE"),
            "TYPE" => "STRING",
            "DEFAULT" => "COOKIE_AGREE",
        ),
        "COOKIE_TEXT"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_TEXT"),
            "TYPE" => "STRING",
            "DEFAULT" => GetMessage("T_TEXT_DEFAULT").$_SERVER['HTTP_HOST'].GetMessage("T_TEXT_DEFAULT"),
        ),
        "COOKIE_TIME"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_TIME"),
            "TYPE" => "STRING",
            "DEFAULT" => "604800",
        ),
        "COOKIE_AGREE_LINK"  =>  Array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("T_LINK"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ),
    ),
);
?>
