<?
$MESS["BIZKIT_MAX_DISCOUNT"] = "Максимальная скидка в %";
$MESS["T_IBLOCK_DESC_GROUP_PERMISSIONS"] = "Группы пользователей, имеющие доступ к детальной информации";
$MESS["CP_BN_CACHE_GROUPS"] = "Учитывать права доступа";
$MESS["CP_BNL_SET_BROWSER_TITLE"] = "Устанавливать заголовок окна браузера";
$MESS["BN_P_IBLOCK_TYPE"] = "Тип инфоблока";
$MESS["BN_P_IBLOCK"] = "Инфоблок";
$MESS["BIZKIT_LIFETIME"] = "Время действия в минутах";
//$MESS["T_IBLOCK_DESC_USE_PERMISSIONS"] = "Только для зарегистрированных пользователей";

?>