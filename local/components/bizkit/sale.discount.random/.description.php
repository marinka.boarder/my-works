<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => GetMessage("BIZKIT_COM_NAME"),
    "DESCRIPTION" => GetMessage("BIZKIT_COM_DESCRIPTION"),
    "ICON" => "",
    "PATH" => array(
        "ID" => "bizkit",
        "SORT" => 10000,
        "NAME" => GetMessage("BIZKIT_MAIN_SECTION"),
    )
);

?>
