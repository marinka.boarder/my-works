<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

/*\Bitrix\Main\Loader::includeModule('iblock');

$iblockTypes = Bitrix\Iblock\TypeTable::getList(array('select' => array('*', 'LANG_MESSAGE')))->FetchAll();
Bitrix\Main\Diag\Debug::writeToFile(array('$iblockTypes' => '$iblockTypes'), "", "debug.txt");*/




if(!CModule::IncludeModule("iblock"))
    return;
$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock=array();
$rsIBlock = CIBlock::GetList(Array("SORT" => "ASC"), Array("TYPE" => $arCurrentValues["IBLOCK_TYPE"], "ACTIVE"=>"Y"));
while($arr=$rsIBlock->Fetch())
{
    $arIBlock[$arr["ID"]] = "[".$arr["ID"]."] ".$arr["NAME"];
}

$arUGroupsEx = Array();
$dbUGroups = CGroup::GetList();
while($arUGroups = $dbUGroups -> Fetch())
{
    $arUGroupsEx[$arUGroups["ID"]] = $arUGroups["NAME"];
}

$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "IBLOCK_TYPE" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("BN_P_IBLOCK_TYPE"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockType,
            "REFRESH" => "Y",
        ),
        "IBLOCK_ID" => array(
            "PARENT" => "BASE",
            "NAME" => GetMessage("BN_P_IBLOCK"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlock,
            "REFRESH" => "Y",
            "ADDITIONAL_VALUES" => "Y",
        ),
        "MAX_DISCOUNT" => array(
            "NAME" => GetMessage("BIZKIT_MAX_DISCOUNT"),
            "TYPE" => "INTEGER",
            "PARENT" => "BASE",
            "MULTIPLE" => "N",
            "REFRESH" => "N",
            "DEFAULT" => "50",
        ),
        "LIFETIME" => array(
            "NAME" => GetMessage("BIZKIT_LIFETIME"),
            "TYPE" => "INTEGER",
            "PARENT" => "BASE",
            "MULTIPLE" => "N",
            "REFRESH" => "N",
            "DEFAULT" => "60",
        ),
        "GROUP_PERMISSIONS" => Array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("T_IBLOCK_DESC_GROUP_PERMISSIONS"),
            "TYPE" => "LIST",
            "VALUES" => $arUGroupsEx,
            "DEFAULT" => Array(1),
            "MULTIPLE" => "Y",
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>36000000),
        "SET_TITLE" => array(),
        "SET_BROWSER_TITLE" => array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("CP_BNL_SET_BROWSER_TITLE"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        /*"CACHE_FILTER" => array(
            "PARENT" => "CACHE_SETTINGS",
            "NAME" => GetMessage("BN_P_CACHE_FILTER"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),*/
        "CACHE_GROUPS" => array(
            "PARENT" => "CACHE_SETTINGS",
            "NAME" => GetMessage("CP_BN_CACHE_GROUPS"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        /*"USE_PERMISSIONS" => Array(
            "PARENT" => "ADDITIONAL_SETTINGS",
            "NAME" => GetMessage("T_IBLOCK_DESC_USE_PERMISSIONS"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
            "REFRESH" => "Y",
        ),*/
    ),
);

?>