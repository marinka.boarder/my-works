<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

use Bitrix\Main\Context,
    Bitrix\Main\Type\DateTime,
    Bitrix\Main\Loader,
    Bitrix\Iblock;

if(!isset($arParams["CACHE_TIME"]))
    $arParams["CACHE_TIME"] = 36000000;

if($arParams["FILTER_NAME"] == '' || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
    $arrFilter = array();
}
else
{
    $arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
    if(!is_array($arrFilter))
        $arrFilter = array();
}

$arParams["CACHE_FILTER"] = $arParams["CACHE_FILTER"]=="Y";
if(!$arParams["CACHE_FILTER"] && count($arrFilter)>0)
    $arParams["CACHE_TIME"] = 0;

$arParams["SET_TITLE"] = $arParams["SET_TITLE"]!="N";
$arParams["SET_BROWSER_TITLE"] = (isset($arParams["SET_BROWSER_TITLE"]) && $arParams["SET_BROWSER_TITLE"] === 'N' ? 'N' : 'Y');

if (empty($arParams["PAGER_PARAMS_NAME"]) || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["PAGER_PARAMS_NAME"]))
{
    $pagerParameters = array();
}
else
{
    $pagerParameters = $GLOBALS[$arParams["PAGER_PARAMS_NAME"]];
    if (!is_array($pagerParameters))
        $pagerParameters = array();
}

$arParams["USE_PERMISSIONS"] = ($arParams["USE_PERMISSIONS"] ?? '') == "Y";
if(!is_array(($arParams["GROUP_PERMISSIONS"] ?? null)))
    $arParams["GROUP_PERMISSIONS"] = array(1);

$bUSER_HAVE_ACCESS = !$arParams["USE_PERMISSIONS"];
if($arParams["USE_PERMISSIONS"] && isset($GLOBALS["USER"]) && is_object($GLOBALS["USER"]))
{
    $arUserGroupArray = $USER->GetUserGroupArray();
    foreach($arParams["GROUP_PERMISSIONS"] as $PERM)
    {
        if(in_array($PERM, $arUserGroupArray))
        {
            $bUSER_HAVE_ACCESS = true;
            break;
        }
    }
}

if($arParams["LIFETIME"] < 1 )
    $arParams["LIFETIME"] = 60;

if(isset($_REQUEST["checkDiscount"]))
    $_REQUEST["checkDiscount"] = trim($_REQUEST["checkDiscount"], ' ');


if($this->startResultCache(false, array(($arParams["CACHE_GROUPS"]==="N"? false: $USER->GetGroups()), $bUSER_HAVE_ACCESS, false, $arrFilter, $pagerParameters))
    && (isset($_REQUEST["getDiscount"]) || isset($_REQUEST["checkDiscount"])))
{
    if(!Loader::includeModule("iblock"))
    {
        $this->abortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
    }

    if(is_numeric($arParams["IBLOCK_ID"]))
    {
        $rsIBlock = CIBlock::GetList(array(), array(
            "ACTIVE" => "Y",
            "ID" => $arParams["IBLOCK_ID"],
        ));
    }
    else
    {
        $rsIBlock = CIBlock::GetList(array(), array(
            "ACTIVE" => "Y",
            "CODE" => $arParams["IBLOCK_ID"],
            "SITE_ID" => SITE_ID,
        ));
    }

    $arResult = $rsIBlock->GetNext();
    if (!$arResult)
    {
        $this->abortResultCache();
        Iblock\Component\Tools::process404(
            trim($arParams["MESSAGE_404"]) ?: GetMessage("T_NEWS_NEWS_NA")
            ,true
            ,$arParams["SET_STATUS_404"] === "Y"
            ,$arParams["SHOW_404"] === "Y"
            ,$arParams["FILE_404"]
        );
        return;
    }

    $idUser =$USER->GetID();

    $arResult["USER_HAVE_ACCESS"] = $bUSER_HAVE_ACCESS;

    $arSort = array('SORT' => 'ASC', 'ID' => 'DESC');

    $shortSelect = array('ID', 'IBLOCK_ID');
    foreach (array_keys($arSort) as $index)
    {
        if (!in_array($index, $shortSelect))
        {
            $shortSelect[] = $index;
        }
    }

    /*   смотрим, есть ли действующая скидка у данного пользователя   */
    $arSelect = array(
        "ID",
        "IBLOCK_ID",
        "IBLOCK_SECTION_ID",
        "NAME",
        "DATE_CREATE",
        "TIMESTAMP_X",
        "DATE_ACTIVE_TO"
    );

    /*          проверка при нажатии "Получить скидку", есть ли уже живая запись          */
    if(isset($_REQUEST["getDiscount"]) ){
        $arFilter = array (
            "IBLOCK_ID"     => $arResult["ID"],
            "IBLOCK_LID"    => SITE_ID,
            "ACTIVE"        => "Y",
            "NAME"          => $idUser .'_'.  $arParams["LIFETIME"],
            ">=DATE_ACTIVE_TO"   => ConvertTimeStamp(false, "FULL"),
        );
    }else{
        /*          проверка времени жизни купона          */
        $arFilter = array (
            "IBLOCK_ID"     => $arResult["ID"],
            "IBLOCK_LID"    => SITE_ID,
            "ACTIVE"        => "Y",
            "NAME"          => $idUser .'_'.  $arParams["LIFETIME"],
            ">=DATE_CREATE" => ConvertTimeStamp(time()-$arParams["LIFETIME"]*180, "FULL"),
            "PROPERTY_COUPON" => $_REQUEST["inputCode"],
            );
    }

    $arResult["ITEMS"] = array();
    $arResult["ELEMENTS"] = array();
    $rsElement = CIBlockElement::GetList($arSort, array_merge($arFilter , $arrFilter), false, false, $arSelect);
    if ($row = $rsElement->Fetch())
    {
        $arResult["ITEMS"] = $row;
    }
    unset($row);

    if (!empty($arResult['ITEMS']))
    {
        $db_props = CIBlockElement::GetProperty($arResult["ITEMS"]["IBLOCK_ID"], $arResult["ITEMS"]["ID"], array("sort" => "asc"), Array("CODE"=>"PERCENT"));
        if($ar_props = $db_props->Fetch())
            $arResult["DISCOUNT"]["PERCENT"] = $ar_props["VALUE"];

        $db_props = CIBlockElement::GetProperty($arResult["ITEMS"]["IBLOCK_ID"], $arResult["ITEMS"]["ID"], array("sort" => "asc"), Array("CODE"=>"COUPON"));
        if($ar_props = $db_props->Fetch())
            $arResult["DISCOUNT"]["COUPON"] = $ar_props["VALUE"];

    }elseif(!isset($_REQUEST["checkDiscount"]) || $_REQUEST["checkDiscount"] == "N"){
            /*   если нет скидки и это не проверка купона, формируем рандомный процент и код скидки   */
            $arResult["DISCOUNT"]["PERCENT"] = rand(1, $arParams["MAX_DISCOUNT"]);
            $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $arResult["DISCOUNT"]["COUPON"] = substr(str_shuffle($permitted_chars), 0, 16);

            $el = new CIBlockElement;

            $PROP = array();
            $PROP["PERCENT"] = $arResult["DISCOUNT"]["PERCENT"];
            $PROP["COUPON"] = $arResult["DISCOUNT"]["COUPON"];


            $arLoadProductArray = Array(
                "MODIFIED_BY"    => $idUser,
                "IBLOCK_SECTION_ID" => false,
                "IBLOCK_ID"      => $arParams["IBLOCK_ID"],
                "PROPERTY_VALUES"=> $PROP,
                "NAME"           => $idUser .'_'.  $arParams["LIFETIME"],
                "ACTIVE"         => "Y",
                "ACTIVE_FROM"    => ConvertTimeStamp(false, "FULL"),
                "ACTIVE_TO"      => ConvertTimeStamp(time()+$arParams["LIFETIME"]*60, "FULL"),
            );

            $PRODUCT_ID = $el->Add($arLoadProductArray);
    }
}

if(isset($arResult))
{
    if($arParams["SET_TITLE"])
    {
        $APPLICATION->SetTitle($arResult["NAME"], $arTitleOptions);
    }

    $this->IncludeComponentTemplate();
}


?>
