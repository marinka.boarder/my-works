<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arResult */
?>
<?if(!isset($arResult["DISCOUNT"])):?>
    <form class="mb-5" action="" method="get">
        <input type="hidden" name="getDiscount" value="Y" />
        <input type="hidden" name="checkDiscount" value="N" />
        <button type="submit" class="btn btn-primary"><?=GetMessage("BIZKIT_GET_DISCOUNT")?></button>
    </form>

    <form class="form" action="" method="get">
        <div class="form-group mb-2">
            <div class="row">
                <div class="col">
                    <input type="text" class="form-control" name="inputCode" id="inputCode" placeholder="Введите код купона">
                </div>
                <div class="col-5">
                    <input type="hidden" name="checkDiscount" value="Y" />
                    <button type="submit" class="btn btn-primary mb-2"><?=GetMessage("BIZKIT_CHECK_DISCOUNT")?></button>
                </div>
            </div>
        </div>
    </form>
    <?if(count($arResult["ITEMS"]) == 0 && isset($_REQUEST["inputCode"])):?>
        <dl class="row">
            <dd class="col-sm-6"><?=$_REQUEST["inputCode"]?></dd>
            <dt class="col-sm-6"><?=GetMessage("BIZKIT_NO_DISCOUNT")?></dt>
        </dl>
    <?endif;?>
<?else:?>
    <dl class="row">
        <dt class="col-sm-3"><?=GetMessage("BIZKIT_PERCENT_DISCOUNT")?></dt>
        <dd class="col-sm-9"><?=$arResult["DISCOUNT"]["PERCENT"];?></dd>

        <dt class="col-sm-3"><?=GetMessage("BIZKIT_UNIQUE_COUPON")?></dt>
        <dd class="col-sm-9"><?=$arResult["DISCOUNT"]["COUPON"];?></dd>
    </dl>
<?endif;?>





