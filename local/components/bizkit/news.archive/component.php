<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
global $USER;
$arGroups = $USER->GetUserGroupArray();

$arParams["CACHE_TIME"] = intval($arParams["CACHE_TIME"]);
if($arParams["CACHE_TIME"]>0)
	$arParams["CACHE_TIME"] = $arParams["CACHE_TIME"];
else
	$arParams["CACHE_TIME"] = 3600;

function know_cnt_elements($date, $t_start, $t_end, $IBLOCK_ID)
{
	$KOLVO = 0;
	$arOrder = array("PROPERTY_START_DATE" => "ASC");
    /*$arFilter = array(
        "IBLOCK_ID" => $IBLOCK_ID,
        "ACTIVE" => "Y",
        ">=".$variant => ($t_start),
        "<=".$variant => ($t_end),
    );*/
    if ((date("Y-m-d") < $t_end)) $t_end = date("Y-m-d");
    $arFilter = Array(array(
        "LOGIC" => "OR",
        array(
            "IBLOCK_ID" => $IBLOCK_ID,
            "ACTIVE" => "Y",
            'PROPERTY_END_DATE' => false,
           // '<PROPERTY_START_DATE' => (date("Y-m-d")),
            '<PROPERTY_START_DATE' => ($t_end),

            '>=PROPERTY_START_DATE' => ($t_start)),
        array(
            "IBLOCK_ID" => $IBLOCK_ID,
            "ACTIVE" => "Y",

            '<PROPERTY_END_DATE' => ($t_end),
           // '<PROPERTY_END_DATE' => (date("Y-m-d")),
            '>=PROPERTY_END_DATE' => ($t_start))
    ));

	$arNavStartParams = array("nPageSize" => 100000);
	$arSelect = array("ID", "PROPERTY_START_DATE", "PROPERTY_END_DATE");
	$resDB = CIBlockElement::GetList($arOrder, $arFilter, false, $arNavStartParams, $arSelect);
	while($arFields = $resDB->Fetch())
	{
		$KOLVO++;
	}
	return $KOLVO;
}

if ($this->StartResultCache( $arParams["CACHE_TIME"] )) {
    CModule::IncludeModule('iblock');

    $ERROR_DETECTED = false;
    /*-------------------------*/

    // $ok = false;
    //$arResult["IBLOCKS"] = array();
    $arResult["IBLOCK_IDS"] = array();

    if (is_numeric($arParams["IBLOCK_TYPE"])) {
        $rsIBlock = CIBlock::GetList(array(), array(
            "ACTIVE" => "Y",
            "ID" => $arParams["IBLOCK_IDS"],
        ));
    } else {
        $rsIBlock = CIBlock::GetList(array(), array(
            "ACTIVE" => "Y",
            "TYPE" => $arParams["IBLOCK_TYPE"],
            "SITE_ID" => SITE_ID,
        ));
    }
    while ($Iblock = $rsIBlock->GetNext()) {
        // $ok = true;
        // $Iblock["USER_HAVE_ACCESS"] = $bUSER_HAVE_ACCESS;
        // $Iblock["SECTION"] = false;

        //  $arResult["IBLOCKS"][$Iblock] = $Iblock;
        $arResult["IBLOCK_IDS"][] = $Iblock["ID"];
    }



   // foreach ($arResult["IBLOCK_IDS"] as $IBLOCK_ID) {
    //Bitrix\Main\Diag\Debug::writeToFile(array('$arParams' => $arParams), "", "debug.txt");
         $arParams["IBLOCK_ID"] = intval($arParams["IBLOCK_ID"]);
        //if($arParams["IBLOCK_ID"]>0)
        $IBLOCK_ID = $arParams["IBLOCK_ID"];
        /*else
        {
            ShowError( GetMessage("IMYIE_ERROR_NO_IBLOCK_ID") );
            $ERROR_DETECTED = true;
            $this->AbortResultCache();
        }*/

        $KNOW_CNT_ELEMENTS = "N";
        if ($arParams["KNOW_CNT_ELEMENTS"] == "Y")
            $KNOW_CNT_ELEMENTS = "Y";

        if (!$ERROR_DETECTED) {
            $arParams["CNT_MONTH"] = intval($arParams["CNT_MONTH"]);
            if ($arParams["CNT_MONTH"] > 0)
                $CNT_LAST_MONTH = $arParams["CNT_MONTH"];
            else
                $CNT_LAST_MONTH = 10;

           /* switch ($arParams["ORDERT_VARIANT"]) {
                case "DATE_ACTIVE_FROM":
                    $TIME_VARRIENT = "DATE_ACTIVE_FROM";
                    break;
                case "DATE_ACTIVE_TO":
                    $TIME_VARRIENT = "DATE_ACTIVE_TO";
                    break;
                case "TIMESTAMP_X":
                    $TIME_VARRIENT = "TIMESTAMP_X";
                    break;
                default:
                    $TIME_VARRIENT = "DATE_ACTIVE_FROM";
                    $arParams["ORDERT_VARIANT"] = "DATE_ACTIVE_FROM";
                    break;
            }*/
            $TIME_VARRIENT = "PROPERTY_START_DATE";
            $date_now = time();
            $arData = array();

            $date = date("Y-m-d", $date_now);
            $date2 = explode('-', $date);
            $date_year = $date2[0];
            $date_month = $date2[1];
            $date_day = 1;
            if(count(array_intersect($arGroups, array(9,10))) > 0){

                global $apl_accepted;
                $arSelect = Array("ID");
                $arFilter = Array("IBLOCK_ID"=>IBLOCK_ID_STUDENTS, "PROPERTY_USER"=>$USER->GetID(), "ACTIVE"=>"Y");
                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                if($ob = $res->GetNext())
                    $userID = $ob["ID"];

                $res = CIBlock::GetList(Array(),Array('TYPE'=>'Events','SITE_ID'=>SITE_ID,'ACTIVE'=>'Y'), true);
                while($ar_res = $res->Fetch())
                    $iblock_events[] = $ar_res['ID'];
                //Bitrix\Main\Diag\Debug::writeToFile(array('$iblock_events' => $iblock_events ),"","debug.txt");
                foreach ($iblock_events as $ibid)
                {
                    $arSelect = Array("ID", "PROPERTY_EVENT_ID");
                    $arFilter = Array("IBLOCK_ID"=>$ibid, "PROPERTY_APPLICANTS"=>$userID, "ACTIVE"=>"Y");
                    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                    while($ob = $res->GetNext())
                        $arr_id[] = $ob["ID"];
                    //Bitrix\Main\Diag\Debug::writeToFile(array('$apl_accepted[$ibid]' => $apl_accepted[$ibid] ),"","debug.txt");

                }
                if(count(array_intersect($arGroups, array(9,10))) > 0)
                    $ID_Filter = $arr_id;

            }
            for ($i = 0; $i < $CNT_LAST_MONTH; $i++) {
                $date_unix = mktime(0, 0, 0, $date_month, $date_day, $date_year);
                $next_unix = mktime(0, 0, 0, ($date_month + 1), $date_day, $date_year);
                if ($arParams["ONLY_ACTIVE_ELEMENTS"] == "Y" && $next_unix > $date_now) {
                    $next_unix = $date_now;
                }
                $start_date = date("Y-m-d", $date_unix);
                $end_date = date("Y-m-d", $next_unix);

                $arOrder = array($TIME_VARRIENT => "ASC");
                /*$arFilter = array(
                    "IBLOCK_ID" => $IBLOCK_ID,
                    "ACTIVE" => "Y",
                    ">=" . $TIME_VARRIENT => ($start_date),
                    "<" . $TIME_VARRIENT => ($end_date),
                );*/

                $arFilter = Array(array(
                    "LOGIC" => "OR",
                    array(
                        "IBLOCK_ID" => $IBLOCK_ID,
                        "ACTIVE" => "Y",
                        'PROPERTY_END_DATE' => false,
                        '<PROPERTY_START_DATE' => ($end_date),
                        '>=PROPERTY_START_DATE' => ($start_date)),
                    array(
                        "IBLOCK_ID" => $IBLOCK_ID,
                        "ACTIVE" => "Y",
                        '<PROPERTY_END_DATE' => ($end_date),
                        '>=PROPERTY_END_DATE' => ($start_date))
                ));
                $arFilter["ID"] = $ID_Filter;
                //Bitrix\Main\Diag\Debug::writeToFile(array('$arFilter' => $arFilter), "", "debug.txt");

                $arNavStartParams = array("nPageSize" => 1);
                $arSelect = array("ID", $TIME_VARRIENT);
                $resDB = CIBlockElement::GetList($arOrder, $arFilter, false, $arNavStartParams, $arSelect);
                if ($arFields = $resDB->Fetch()) {
                    $CCNNTT = 0;

                    $dateForData = date("Y-m-d", $date_unix);
                    $dateForData2 = explode('-', $dateForData);
                    if ($KNOW_CNT_ELEMENTS == "Y")
                        $CCNNTT = know_cnt_elements($date, $start_date, $end_date, $IBLOCK_ID);

                    $arData[] = array(
                        "UNIX" => $date_unix,
                        "DATE_FORMATED" => array(
                            "DAY" => $dateForData2[2],
                            "MONTH" => $dateForData2[1],
                            "MONTH_NAME" => GetMessage("IMYIE_MONTH_NAME_" . $dateForData2[1]),
                            "YEAR" => $dateForData2[0],
                        ),
                        "ISSET_ELEMENTS" => "Y",
                        "CNT" => $CCNNTT,
                    );
                    $date1 = ConvertDateTime($arFields[$TIME_VARRIENT], "YYYY-MM-DD", "ru");
                } else {
                    $CCNNTT = 0;
                    $dateForData = date("Y-m-d", $date_unix);
                    $dateForData2 = explode('-', $dateForData);
                    $arData[] = array(
                        "UNIX" => $date_unix,
                        "DATE_FORMATED" => array(
                            "DAY" => $dateForData2[2],
                            "MONTH" => $dateForData2[1],
                            "MONTH_NAME" => GetMessage("IMYIE_MONTH_NAME_" . $dateForData2[1]),
                            "YEAR" => $dateForData2[0],
                        ),
                        "ISSET_ELEMENTS" => "N",
                        "CNT" => $CCNNTT,
                    );
                }
                $date_month--;
            }
           // Bitrix\Main\Diag\Debug::writeToFile(array('$arData' => $arData), "", "debug.txt");
            $arResult["MONTH"] = $arData;
            $this->IncludeComponentTemplate();
        }
   // }

}


?>