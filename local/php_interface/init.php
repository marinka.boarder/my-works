<?php

//Константы
require dirname(__FILE__) . '/constants.php';

//Автозагрузка классов
require dirname(__FILE__) . '/autoload.php';

//Обработка событий
require dirname(__FILE__) . '/event_handler.php';

/*    смс напоминание о старте аукциона      */
function Notify_Start_Auction()
{
    $filter = Array(
        "ACTIVE"              => "Y",
        "GROUPS_ID" => Array(1,12)
    );

    $rsUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
    while ($arUser = $rsUsers->Fetch()) {
        if(stristr($arUser["LOGIN"], '+7')) {
            $ch = curl_init("https://api.etpagro.ru/sendsms.php");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_TIMEOUT, 30);
            $Fild = http_build_query(array(
                                         "api_id" => "A0679-348F-E5A2-E06C-58885",
                                         "phone_number" => preg_replace("#[^0-9]*#","",$arUser["LOGIN"]),
                                         "message" => 'Старт аукциона на сайте https://sht.tdab.ru.',
                                         "json" => 1
                                     ));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $Fild);
            $body = curl_exec($ch);
            curl_close($ch);
            $json = json_decode($body);
        }
    }
    return "Notify_Start_Auction();";
}
/*        отсылаем e-mail с файлом клиенту           */
function Send_mailto($to_client_email, $owner_mail, $body, $file_path, $eventName){
    $return_code = 0;

    $arSendFields = array(
        "TEXT"             => $body,
        "EMAIL"            => $to_client_email,
        "SALE_EMAIL"       => $owner_mail,

    );

    $event = new CEvent;
    $event->Send($eventName, "s1", $arSendFields, "N", "", array($file_path));

    return $return_code;
}
/*          генерация пдф заявки           */
function mainPDF($path_to_save_pdf, $html) {
    if (file_exists($_SERVER["DOCUMENT_ROOT"]."/local/templates/sht_new/tcpdf/tcpdf.php")){
        //ob_get_clean();
        require_once($_SERVER["DOCUMENT_ROOT"]."/local/templates/sht_new/tcpdf/tcpdf.php");
    }

    $pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);

    $pdf->SetAuthor('#sht.tdab.ru');
    $pdf->SetTitle('Заявление на приобретение бывшей в употреблении техники');

    $pdf->SetHeaderData("", 0, "#sht.tdab.ru", "Интернет-сервис подачи заявок на приобретение сельскохозяйственной техники бывшей в употреблении");

    $pdf->setHeaderFont(Array('dejavusanscondensed', '', 6));
    $pdf->setFooterFont(Array('dejavusanscondensed', '', 6));

    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    $pdf->SetMargins(18, PDF_MARGIN_TOP, 8);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    $pdf->SetFont('dejavusanscondensed', '', 10);

    $pdf->AddPage();

    $pdf->writeHTML($html, true, false, true, false, '');

    $pdf->Output($path_to_save_pdf, "F");

    return $pdf;
}
/*          генерация эксельки с напоминанием           */
function send_last_product()
{
    CModule::IncludeModule('iblock');

    $month_arr = array(3,4,5);
    $html = '';
    $date_x = date("20-m-Y");
    $notify_3 = 'Напоминаем Вам, что ' . $date_x . ' по следующим позициям заканчивается 3х месячный срок действия установленной цены. Рекомендуется уменьшение розничной цены на 30 % от первоначального размещения.';
    $notify_4 = 'Напоминаем Вам, что ' . $date_x . ' по следующим позициям заканчивается 4х месячный срок действия установленной цены. Рекомендуется уменьшение розничной цены на 10 % от первоначального размещения.';
    $notify_5 = 'Напоминаем Вам, что ' . $date_x . ' по следующим позициям заканчивается 5ти месячный срок действия установленной цены. Рекомендуется уменьшение розничной цены на 10 % от первоначального размещения.';
    foreach($month_arr as $month){
        $arrFilter = array (
            "IBLOCK_ID" => 1,
            "IBLOCK_LID" => SITE_ID,
            "ACTIVE" => "Y"
        );

        $date_1 = date("t.m.Y", strtotime("-" . $month . " month"));
        $date_2 = date("01.m.Y", strtotime("-" . $month . " month"));
        $arrFilter[">=DATE_ACTIVE_FROM"] = $date_2;
        $arrFilter["<=DATE_ACTIVE_FROM"] = $date_1;

        $arSelect = array(
            "ID",
            "NAME",
            "ACTIVE_FROM",
            "TIMESTAMP_X",
            "PROPERTY_CODE"
        );
        $arParams = array();

        $arSort = array(
            $arParams["SORT_BY1"]=>"DATE_ACTIVE_FROM",
            $arParams["SORT_BY2"]=>"NAME",
        );
        $arSort["ID"] = "ASC";

        if($month == 3)
            $title = $notify_3;
        elseif($month == 4)
            $title = $notify_4;
        elseif($month == 5)
            $title = $notify_5;
        $body = $title;

        $file_full_path = '';
        $file_name_hash = md5( $month . ' месячный срок' . time());
        $file_name = $file_name_hash . ".xls";
        $file_full_path = $_SERVER["DOCUMENT_ROOT"]."/requests/mail/" . $file_name;
        //Bitrix\Main\Diag\Debug::writeToFile(array('$_SERVER["DOCUMENT_ROOT"]' => $_SERVER["DOCUMENT_ROOT"] ),"","test/debug.txt");

        if (CModule::IncludeModule("nkhost.phpexcel")){
            global $PHPEXCELPATH;
            require_once($PHPEXCELPATH.'/PHPExcel.php');
            require_once($PHPEXCELPATH.'/PHPExcel/Writer/Excel5.php');
            $xls = new PHPExcel();
            $xls->setActiveSheetIndex(0);
            $sheet = $xls->getActiveSheet();
            $sheet->setTitle('Техника');

            $sheet->setCellValue("A1", $title);
            $sheet->mergeCells('A1:H1');
            $sheet->getStyle('A1')->getAlignment()->setHorizontal(
                PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

            $sheet->setCellValue("A2", ' Инвентарный номер');
            $sheet->setCellValue("B2", "Название");
            $sheet->setCellValue("C2", "Начало активности");
            $sheet->getStyle('A2')->getFill()->setFillType(
                PHPExcel_Style_Fill::FILL_SOLID);
            $sheet->getStyle('A2')->getFill()->getStartColor()->setRGB('EEEEEE');
            $sheet->getStyle('B2')->getFill()->setFillType(
                PHPExcel_Style_Fill::FILL_SOLID);
            $sheet->getStyle('B2')->getFill()->getStartColor()->setRGB('EEEEEE');
            $sheet->getStyle('C2')->getFill()->setFillType(
                PHPExcel_Style_Fill::FILL_SOLID);
            $sheet->getStyle('C2')->getFill()->getStartColor()->setRGB('EEEEEE');
            $i = 3;

            $rsElement = CIBlockElement::GetList($arSort, $arrFilter, false, false, $arSelect);
            while($obElement = $rsElement->GetNextElement()) {
                $arItem = $obElement->GetFields();
                //Bitrix\Main\Diag\Debug::writeToFile(array('$arItem' => $arItem ),"","test/debug.txt");
                $sheet->setCellValue("A" . $i, html_entity_decode($arItem["PROPERTY_CODE_VALUE"]));
                $sheet->setCellValue("B" . $i, html_entity_decode($arItem["NAME"]));
                $sheet->setCellValue("C" . $i, html_entity_decode($arItem["ACTIVE_FROM"]));
                $i++;
            }

            $objWriter = new PHPExcel_Writer_Excel5($xls);
            $objWriter->save( $file_full_path);
        }

        $to_client_email = "technics@etpagro.ru, dirtrans@agrobel.ru";
        $owner_mail = "noreply@tdab.ru";
        $eventName = "NOTIFY_CHANGE_PRICE";
        //Bitrix\Main\Diag\Debug::writeToFile(array('$html' => $html ),"","test/debug.txt");
        Send_mailto($to_client_email, $owner_mail, $body, $file_full_path, $eventName);
    }

    return "send_last_product();";
}
/*          снятие с брони, если прошло больше 10 дней           */
function unset_booking()
{
    CModule::IncludeModule('iblock');

    $arSelect = Array("ID", "NAME", "PROPERTY_BOOKING", "PROPERTY_DATA_BOOKING");
    $arFilter = Array("IBLOCK_ID"=>CATALOG_IBLOCK, "ACTIVE"=>"Y", "PROPERTY_BOOKING_VALUE" => 'Да');
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($Fields = $res->GetNext())
    {
        $today = date('d.m.Y');
        $datetime1 = new DateTime($today);
        $datetime2 = new DateTime($Fields["PROPERTY_DATA_BOOKING_VALUE"]);
        $interval = date_diff($datetime1, $datetime2);
        //
        if($interval->days >= 10)
            CIBlockElement::SetPropertyValuesEx(
                $Fields["ID"],
                CATALOG_IBLOCK,
                array(
                    "BOOKING" =>  '',
                    "DATA_BOOKING" =>  ''));
    }
    return "unset_booking();";
}
