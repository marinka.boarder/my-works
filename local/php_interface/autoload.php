<?php

use Bitrix\Main\Loader;

Loader::registerAutoLoadClasses(null, [
    'lib\UserType\CUserTypeUserId' => APP_CLASS_FOLDER . 'UserType/CUserTypeUserId.php',
    'lib\UserType\CUserTypeTextarea' => APP_CLASS_FOLDER . 'UserType/CUserTypeTextarea.php'
]);