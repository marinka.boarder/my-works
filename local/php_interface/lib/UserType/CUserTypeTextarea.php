<?


namespace lib\usertype;

use \Bitrix\Main,
    \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\UserField,
    \Bitrix\Main\Page\Asset;

class CUserTypeTextarea
{
    public function GetUserTypeDescription()
	{
		return array(
			"USER_TYPE_ID" => "customhtml",
			"CLASS_NAME" => __CLASS__,
			"DESCRIPTION" => "HTML/text",
			"BASE_TYPE" => \CUserTypeManager::BASE_TYPE_STRING,
		);
	}

    public function GetEditFormHTML($arUserField, $arHtmlControl)
	{
        if($arUserField["ENTITY_VALUE_ID"]<1 && strlen($arUserField["SETTINGS"]["DEFAULT_VALUE"])>0)
			$arHtmlControl["VALUE"] = htmlspecialcharsbx($arUserField["SETTINGS"]["DEFAULT_VALUE"]);
		if($arUserField["SETTINGS"]["ROWS"] < 8)
			$arUserField["SETTINGS"]["ROWS"] = 8;

		$name = $arHtmlControl["NAME"];

		ob_start();

		\CFileMan::AddHTMLEditorFrame(
			"EDITOR",
			$arHtmlControl["VALUE"],
			$name."_TYPE",
			strlen($arHtmlControl["VALUE"])?"html":"text",
			array(
				'height' => $arUserField['SETTINGS']['ROWS']*10,
			)
		);

		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}



    public function GetEditFormHTMLMulty($arUserField, $arHtmlControl)
    {
        if($arUserField["ENTITY_VALUE_ID"]<1 && strlen($arUserField["SETTINGS"]["DEFAULT_VALUE"])>0)
            $arHtmlControl["VALUE"] = htmlspecialcharsbx($arUserField["SETTINGS"]["DEFAULT_VALUE"]);
        if($arUserField["SETTINGS"]["ROWS"] < 8)
            $arUserField["SETTINGS"]["ROWS"] = 8;


        $name = preg_replace("/[\[\]]/i", "_", $arHtmlControl["NAME"]);


        ob_start();

        \CFileMan::AddHTMLEditorFrame(
            "EDITOR",
            $arHtmlControl["VALUE"],
            $name."_TYPE",
            strlen($arHtmlControl["VALUE"])?"html":"text",
            array(
                'height' => $arUserField['SETTINGS']['ROWS']*10,
            )
        );

        echo '<input type="hidden" name="'.$arHtmlControl["NAME"].'" >';

        $html = ob_get_contents();
        ob_end_clean();

        return $html;

    }


    public function GetFilterHTML($arUserField, $arHtmlControl) {
        $sVal = intval($arHtmlControl['VALUE']);
        $sVal = $sVal > 0 ? $sVal : '';

        return CUserTypeTextarea::GetEditFormHTML($arUserField, $arHtmlControl);
    }

    function GetAdminListViewHTML($arUserField, $arHtmlControl) {
        return '1';
    }

    function GetAdminListViewHTMLMulty($arUserField, $arHtmlControl) {
        return '2';
    }

    function GetAdminListEditHTML($arUserField, $arHtmlControl) {
        return '3';
    }

    function GetAdminListEditHTMLMulty($arUserField, $arHtmlControl) {
        return '4';
    }

    function OnSearchIndex($arUserField) {
        return '5';
    }


    static function OnBeforeSave($arUserField, $value)
    {
		if($arUserField['MULTIPLE'] == 'Y')
		{
			foreach($_POST as $key => $val)
			{
				if( preg_match("/".$arUserField['FIELD_NAME']."_([0-9]+)_$/i", $key, $m) )
				{
					$value = $val;
					unset($_POST[$key]);
					break;
				}
			}
		}
        return $value;
    }
}
?>